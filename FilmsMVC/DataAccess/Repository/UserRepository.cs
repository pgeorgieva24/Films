﻿using DataAccess.DbAccess;
using DataAccess.Entity;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Repository
{
    public class UserRepository 
    {
        protected DbContext Context { get; set; }
        protected DbSet<User> Items { get; set; }

        public UserRepository()
        {
            Context = new ApplicationDbContext();
            Items = Context.Set<User>();
        }

        public User GetById(string id)
        {
            return Items.Where(i => i.Id == id).FirstOrDefault();
        }

        public List<User> GetAll()
        {
            return Items.ToList();
        }

        public void Save(User item)
        {
            if (item.Id ==null)
                Items.Add(item);
            else
                Context.Entry(item).State = EntityState.Modified;

            Context.SaveChanges();
        }

        public void Delete(User item)
        {
            Items.Remove(item);

            Context.SaveChanges();
        }
    }
}
