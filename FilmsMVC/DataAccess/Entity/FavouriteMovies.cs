﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace DataAccess.Entity
{
    public class FavouriteMovies : BaseEntity
    {
        [ForeignKey("User")]
        public string UserId { get; set; }
        [ForeignKey("Film")]
        public int FilmId { get; set; }

        public virtual List<Film> Film { get; set; }
        public virtual List<User> User { get; set; }

    }
}
