﻿using System.Collections.Generic;

namespace DataAccess.Entity
{
    public class Genre : BaseEntity
    {
        public string Name { get; set; }

        public virtual List<Film> Films { get; set; }
    }
}