﻿using System.Collections.Generic;

namespace DataAccess.Entity
{
    public class Actor : BaseEntity
    {
        public string FullName { get; set; }

        public virtual List<Film> Films { get; set; }
    }
}