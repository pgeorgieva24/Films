﻿using System.ComponentModel.DataAnnotations.Schema;

namespace DataAccess.Entity
{
    public class Comment : BaseEntity
    {
        [ForeignKey("Film")]
        public int FilmId { get; set; }
        [ForeignKey("User")]
        public string UserId { get; set; }
        public string Text { get; set; }

        public virtual Film Film { get; set; }
        public virtual User User { get; set; }
    }
}