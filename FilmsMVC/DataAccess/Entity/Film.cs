﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace DataAccess.Entity
{
    public class Film : BaseEntity
    {
        public string Title { get; set; }
        public int Year { get; set; }
        public string Summary { get; set; }
        
        public virtual List<Actor> Actors { get; set; }
        public virtual List<Genre> Genres { get; set; }
        public virtual List<Image> Image { get; set; }
        public virtual List<Comment> Comment { get; set; }
        //public virtual FavouriteMovies FavouriteMovie { get; set; }
    }
}
