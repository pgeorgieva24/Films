﻿using System.ComponentModel.DataAnnotations.Schema;

namespace DataAccess.Entity
{
    public class Image : BaseEntity
    {
        public string Title { get; set; }
        public string ImgUrl { get; set; }
        [ForeignKey("Film")]
        public int FilmId { get; set; }
        
        public virtual Film Film { get; set; }
    }
}