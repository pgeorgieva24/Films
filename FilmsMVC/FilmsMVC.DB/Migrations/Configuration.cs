namespace FilmsMVC.DB.Migrations
{
    using System.Collections.Generic;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using DataAccess.Entity;

    internal sealed class Configuration : DbMigrationsConfiguration<FilmsMVC.DB.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
        }

        protected override void Seed(FilmsMVC.DB.ApplicationDbContext context)
        {
            
            var films = new List<Film>
            {
                new Film
                {
                    Title = "Drive",
                    Year = 2011,
                    Summary = "A Hollywood stunt performer who moonlights as a wheelman for criminals discovers that a contract has been put on him after a heist gone wrong.",
                    Comments = new List<Comment>(),
                    Genres = new List<Genre>(),
                    Images = new List<Image>()
                },
                new Film
                {
                    Title = "The Big Short",
                    Year = 2011,
                    Summary = "THIS IS A TRUE STORY. The men who made millions from a global economic meltdown.",
                    Comments = new List<Comment>(),
                    Genres = new List<Genre>(),
                    Images = new List<Image>()
                },
                new Film
                {
                    Title = "Fight Club",
                    Year = 1999,
                    Summary = "MISCHIEF. MAYHEM. SOAP. A ticking-time-bomb insomniac and a slippery soap salesman channel primal male aggression into a shocking new form of therapy. Their concept catches on, with underground �fight clubs� forming in every town, until an eccentric gets in the way and ignites an out-of-control spiral toward oblivion.",
                    Comments = new List<Comment>(),
                    Genres = new List<Genre>(),
                    Images = new List<Image>()
                }
            };

            films.ForEach(f => context.Films.AddOrUpdate(p => p.Title, f));
            context.SaveChanges();

            var images = new List<Image>
            {
                new Image
                {
                    FilmId=films.FirstOrDefault(f=>f.Title=="Drive").Id,
                    ImgUrl=
                }
            }
            var genres = new List<Genre>
            {
                new Genre { Name = "Action", Films = new List<Film>() },
                new Genre { Name = "Comedy", Films = new List<Film>() },
                new Genre { Name = "Drama", Films = new List<Film>() },
                new Genre { Name = "Fantasy", Films = new List<Film>() },
                new Genre { Name = "Romance", Films = new List<Film>() },
                new Genre { Name = "Science fiction", Films = new List<Film>() },
                new Genre { Name = "Thriller", Films = new List<Film>() }
            };

            genres.ForEach(g => context.Genres.AddOrUpdate(p => p.Name, g));
            context.SaveChanges();

            var actors = new List<Actor>
            {
                new Actor
                {
                    FullName = "Ryan Gosling",
                    Films = new List<Film>()
                },
                new Actor
                {
                    FullName = "Bryan Cranston",
                    Films = new List<Film>()
                },
                new Actor
                {
                    FullName = "Edward Norton",
                    Films = new List<Film>()
                },
                new Actor
                {
                    FullName = "Brad Pitt",
                    Films = new List<Film>()

                }
            };
            actors.ForEach(f => context.Actors.AddOrUpdate(p => p.FullName, f));
            context.SaveChanges();

            foreach (var item in actors)
            {
                if (item.FullName == "Brad Pitt")
                {
                    item.Films.Add(context.Films.Where(f => f.Title == "Fight Club").FirstOrDefault());
                    item.Films.Add(context.Films.Where(f => f.Title == "The Big Short").FirstOrDefault());
                }
                if (item.FullName == "Edward Norton")
                {
                    item.Films.Add(context.Films.Where(f => f.Title == "Fight Club").FirstOrDefault());
                }
                if (item.FullName == "Bryan Cranston")
                {
                    item.Films.Add(context.Films.Where(f => f.Title == "Drive").FirstOrDefault());
                }
                if (item.FullName == "Ryan Gosling")
                {
                    item.Films.Add(context.Films.Where(f => f.Title == "Drive").FirstOrDefault());
                    item.Films.Add(context.Films.Where(f => f.Title == "The Big Short").FirstOrDefault());
                }
            }

            foreach (var item in genres)
            {
                if (item.Name == "Action")
                {
                    item.Films.Add(context.Films.Where(f => f.Title == "Drive").FirstOrDefault());
                }
                if (item.Name == "Comedy")
                {
                    item.Films.Add(context.Films.Where(f => f.Title == "The Big Short").FirstOrDefault());
                }
                if (item.Name == "Drama")
                {
                    item.Films.Add(context.Films.Where(f => f.Title == "Drive").FirstOrDefault());
                    item.Films.Add(context.Films.Where(f => f.Title == "The Big Short").FirstOrDefault());
                    item.Films.Add(context.Films.Where(f => f.Title == "Fight Club").FirstOrDefault());
                }
                if (item.Name == "Thriller")
                {
                    item.Films.Add(context.Films.Where(f => f.Title == "Drive").FirstOrDefault());
                }
            }
        }
    }
}

