﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using DataAccess.Entity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace FilmsMVC.DB
{
    public class ApplicationDbContext : IdentityDbContext<User>
    {
        public DbSet<Actor> Actors { get; set; }
        public DbSet<Comment> Comments { get; set; }
        public DbSet<FavouriteMovies> FavouriteMovies { get; set; }
        public DbSet<Film> Films { get; set; }
        public DbSet<Genre> Genres { get; set; }
        public DbSet<Image> Images { get; set; }

        public ApplicationDbContext()
            : base(DbConnection.GetLocalConnection(), throwIfV1Schema: false)
        {
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }
        
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Genre>()
            .HasMany<Film>(g => g.Films)
            .WithMany(f => f.Genres)
            .Map(cs =>
            {
                cs.MapLeftKey("Genre_Id");
                cs.MapRightKey("Film_Id");
                cs.ToTable("GenreFilms");
            });

            modelBuilder.Entity<Actor>()
            .HasMany<Film>(a => a.Films)
            .WithMany(f => f.Actors)
            .Map(cs =>
            {
                cs.MapLeftKey("Actor_Id");
                cs.MapRightKey("Film_Id");
                cs.ToTable("FilmActors");
            });
            
        }

    }
}
